//
//  HomeView.swift
//  friends
//
//  Created by AL Mustakim on 22/5/21.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var friendsManager: FriendsManager = FriendsManager()
    let screen = UIScreen.main.bounds.width - 24
    
    var body: some View {
        
        NavigationView {
            //Text("home")
            VStack {
                Text("Meet My Friends").bold().font(.system(size: 18))
                ScrollView {
                    
                    
                    HStack (alignment: .center){
                        
                        VStack {
                            
                            LazyVGrid(columns: [
                                GridItem(.flexible(minimum: screen/4, maximum: screen/3), alignment: .top),
                                GridItem(.flexible(minimum: screen/4, maximum: screen/3), alignment: .top),
                                GridItem(.flexible(minimum: screen/4, maximum: screen/3)),
                            ], alignment: .leading, spacing: 16, content: {
                                if (self.friendsManager.newsLetters.count > 8){
                                ForEach(0..<self.friendsManager.newsLetters.count) { newsLetter in
                                    NavigationLink(destination: friendsDetailView(friend: self.friendsManager.newsLetters[newsLetter] )){
                                        
                                                        HStack{
                                                            friendCardView(friend: self.friendsManager.newsLetters[newsLetter])
                                                            //Spacer().frame(width: 16.0)
                                                        }
                                                        }
                                }
                            }
                                    
                                }).padding(.horizontal, 16)
                            
                            
                            
                        }.padding(.leading)
                        .onAppear(){
                            self.friendsManager.getFriends()
                            
                    }
                        Spacer()
                    }
                }
            }
            
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

