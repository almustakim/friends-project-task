//
//  ContentView.swift
//  friends
//
//  Created by AL Mustakim on 21/5/21.
//

import SwiftUI
import MessageUI

//MARK:- https://www.hackingwithswift.com/books/ios-swiftui/making-navigationview-work-in-landscape



struct ContentView: View {
    
    @ObservedObject var friendsManager: FriendsManager = FriendsManager()
    
    let screen = UIScreen.main.bounds.width - 32

    var body: some View {
        NavigationView{
            ScrollView {
                
            }.onAppear(){
                self.friendsManager.getFriends()
            }
            //.navigationBarTitle("no title")
                             .navigationBarHidden(true)
            
            //WelcomeView()
            ScrollView {
                ZStack{
                    VStack {
                        //HStack {
                            VStack {
                                HStack {
                                LazyVGrid(columns: [
                                    // GridItem(.fixed(50)),
                                    GridItem(.flexible(minimum: screen/4, maximum: screen/3), alignment: .top),
                                    GridItem(.flexible(minimum: screen/4, maximum: screen/3), alignment: .top),
                                    GridItem(.flexible(minimum: screen/4, maximum: screen/3)),
                                ], alignment: .center, spacing: 16, content: {
                                    if (self.friendsManager.newsLetters.count > 8){
                                        //Spacer()
                                    ForEach(0..<self.friendsManager.newsLetters.count) { newsLetter in
                                        NavigationLink(destination: friendCardView(friend: self.friendsManager.newsLetters[newsLetter] )){
                                            //Spacer().frame(width:16)
                                                            HStack{
                                                                
                                                                friendCardView(friend: self.friendsManager.newsLetters[newsLetter])
                                                                //Spacer()
                                                            }
                                            //Spacer().frame(width:16)
                                                            }
                                    }
                                }

    //                                    ForEach(friendsManager.newsLetters){i in
    //
    //                                        NavigationLink(destination: Text("new window")){
    //                                                //NavigationLink(destination: ProductDetailsView(item: i)){
    //
    //                                            ProductItemView( friend: self.friendsManager.newsLetters[i])
    //
    //                                            }
    //
    //                                        }
                                        
                                    }).padding(.horizontal, 16)
                                //Spacer()
                                }
                                
                            }
                            Spacer()
                       // }

                        }
    //                    VStack{
    //                        ScrollView {
    //                                     LazyVGrid(columns: Array(repeating: GridItem(), count: 4)) {
    //                                        ForEach(0..<4) { object in
    //                                             ProductItemView()
    //                                         }
    //                                     }
    //                                 }
    //                    }
                }
            }
                        }
                       // .edgesIgnoringSafeArea(.top)
        .phoneOnlyStackNavigationView()
        
    }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension View {
    func phoneOnlyStackNavigationView() -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return AnyView(self.navigationViewStyle(StackNavigationViewStyle()))
        } else {
            return AnyView(self)
        }
    }
}

struct friendCardView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var cardWidth: CGFloat = (UIScreen.main.bounds.size.width - 160)/3
   // var cardWidth: CGFloat = (850/3)
    var friend : RResult
   // @StateObject var settings = GameSettings()
    
    
    var body: some View {
        
        VStack(alignment: .leading){
            //{baseUrl}/api/v1/api-client/replenishment/get-catalog-item-photo/${product.id}
            VStack {
                AsyncImage(url: URL(string: "\(friend.picture?.thumbnail ?? "")")!,
                           placeholder: { Text("...") },
                           image: { Image(uiImage: $0).resizable() })
                   .frame(idealHeight: UIScreen.main.bounds.width / 2 * 3)
                //Image("")
                    //.frame(width: cardWidth, height: 100).clipped()
            }.frame(width: cardWidth)
            //Image("card_98").frame(width: cardWidth, height: 100).clipped()
            VStack(alignment: .leading){
                Text("\(friend.name?.title ?? "") \(friend.name?.first ?? "") \(friend.name?.last ?? "")").bold().font(.system(size: 13)).lineLimit(2).frame(height: 32)
                Spacer().frame(height: 4)
                VStack(alignment: .leading){
                    Text("Country: \(friend.location?.city ?? "") \(friend.location?.country ?? "")").font(.system(size: 10)).frame(height: 14)
                    Text("").font(.system(size: 10)).frame(height: 14)
                }
                Spacer().frame(height: 8)
                
            }
            .padding(.horizontal, 4.0)
            
        }.frame(width: cardWidth, height: cardWidth)
        //.background(Color(.darkGray))
        .background(self.colorScheme == .light ? Color(#colorLiteral(red: 0.9802958369, green: 0.9804596305, blue: 0.98027426, alpha: 1)) : Color(#colorLiteral(red: 0.08636198193, green: 0.08618018776, blue: 0.09056384116, alpha: 1)) )
        .cornerRadius(8)
        .padding()
        
    }
}


struct friendsDetailView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var cardWidth: CGFloat = (UIScreen.main.bounds.size.width - 160)
   // var cardWidth: CGFloat = (850/3)
    var friend : RResult
    @State var result: Result<MFMailComposeResult, Error>? = nil
    
       @State var isShowingMailView = false
    
    
    var body: some View {
        
        VStack(alignment: .leading){
            //{baseUrl}/api/v1/api-client/replenishment/get-catalog-item-photo/${product.id}
            VStack {
                AsyncImage(url: URL(string: "\(friend.picture?.medium ?? "")")!,
                           placeholder: { Text("...") },
                           image: { Image(uiImage: $0).resizable() })
                   .frame(idealHeight: UIScreen.main.bounds.width / 2 * 3)
                //Image("")
                    //.frame(width: cardWidth, height: 100).clipped()
            }.frame(width: cardWidth)
            //Image("card_98").frame(width: cardWidth, height: 100).clipped()
            VStack(alignment: .leading){
                Text("\(friend.name?.title ?? "") \(friend.name?.first ?? "") \(friend.name?.last ?? "")").bold()
                Spacer().frame(height: 8)
                VStack(alignment: .leading){
                    Text("Address: \(friend.location?.street?.number ?? 0), \(friend.location?.street?.name ?? "")")
                    Text("\(friend.location?.city ?? ""), \(friend.location?.state ?? "") \(friend.location?.country ?? "")")
                    Text("")
                    VStack(alignment: .leading){
                        HStack {
                            Text("Email: ")
                            if MFMailComposeViewController.canSendMail() {
                                            Button("\(friend.email ?? "")") {
                                                self.isShowingMailView.toggle()
                                            } .disabled(!MFMailComposeViewController.canSendMail())
                                            .sheet(isPresented: $isShowingMailView) {
                                                MailView(result: self.$result)
                                        }
                                        } else {
                                            Text("\(friend.email ?? "")")
                                            
                                        }
                            
                        }
                        Text("Phone: \(friend.phone ?? "")")
                    }
                    Spacer().frame(height: 8)
//                    
//                    • Email
//                    • Cell phone
                }
                Spacer().frame(height: 8)
                
            }
            .padding(.horizontal, 4.0)
            
        }.frame(width: cardWidth, height: cardWidth)
        //.background(Color(.darkGray))
        .background(self.colorScheme == .light ? Color(#colorLiteral(red: 0.9802958369, green: 0.9804596305, blue: 0.98027426, alpha: 1)) : Color(#colorLiteral(red: 0.08636198193, green: 0.08618018776, blue: 0.09056384116, alpha: 1)) )
        .cornerRadius(8)
        .padding()
        .navigationTitle("\("\(friend.name?.title ?? "") \(friend.name?.first ?? "") \(friend.name?.last ?? "")")")
    }
}


import SwiftUI
import UIKit
import MessageUI

struct MailView: UIViewControllerRepresentable {

    @Environment(\.presentationMode) var presentation
    @Binding var result: Result<MFMailComposeResult, Error>?

    class Coordinator: NSObject, MFMailComposeViewControllerDelegate {

        @Binding var presentation: PresentationMode
        @Binding var result: Result<MFMailComposeResult, Error>?

        init(presentation: Binding<PresentationMode>,
             result: Binding<Result<MFMailComposeResult, Error>?>) {
            _presentation = presentation
            _result = result
        }

        func mailComposeController(_ controller: MFMailComposeViewController,
                                   didFinishWith result: MFMailComposeResult,
                                   error: Error?) {
            defer {
                $presentation.wrappedValue.dismiss()
            }
            guard error == nil else {
                self.result = .failure(error!)
                return
            }
            self.result = .success(result)
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(presentation: presentation,
                           result: $result)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = context.coordinator
        return vc
    }

    func updateUIViewController(_ uiViewController: MFMailComposeViewController,
                                context: UIViewControllerRepresentableContext<MailView>) {

    }
}
