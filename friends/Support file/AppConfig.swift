//
//  AppConfig.swift
//  friends
//
//  Created by AL Mustakim on 21/5/21.
//

import UIKit
import SwiftUI


let configHostUrl : String = "https://randomuser.me"
let configAPIVersion: String = "/api/"

let configHostAPIUrl: String = configHostUrl + configAPIVersion

let configPrimaryColor = Color(red: 10/255.0, green: 10/255.0, blue: 10/255.0)
let configSecondaryColor  = Color(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0)
