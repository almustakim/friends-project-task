//
//  RequestHandler.swift
//  friends
//
//  Created by AL Mustakim on 21/5/21.
//

import UIKit
import SwiftUI

class RequestHandler: NSObject {

    static func getFriendsByReq(completion: @escaping (_ webResponse: FriendsResponseModel?) -> Void){
        
        var request = URLRequest(url: URL(string: "\(configHostAPIUrl)?page=1&results=10")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            
            return
          }
          print(String(data: data, encoding: .utf8)!)
            do {
                                let decoder = JSONDecoder()
                                decoder.keyDecodingStrategy = .convertFromSnakeCase
                                let dsIdResponse = try decoder.decode(FriendsResponseModel.self, from: data)
                print(dsIdResponse.results?[0])
                completion(dsIdResponse)
                            }
                            catch{
                                print("getUserIdByD JSONSerialization error:", error)
                                completion(nil)
                            }
          
        }

        task.resume()
        
    }
    
    
}

